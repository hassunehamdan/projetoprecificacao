--- PROJETO DE PRECIFICAÇÃO ---
Este projeto consiste em criar um modelo de regressão linear múltipla para prever valores de precificação com base em um conjunto de dados contido no arquivo teste_indicium_precificacao.csv. O projeto é dividido em dois scripts principais: modelo_precificacao.ipynb e previsao_valor_json.ipynb. Além disso, são gerados dois arquivos de modelo: column_transformer.pkl e modelo_linear.pkl.

--- ESTRUTURA DE ARQUIVOS ---
- column_transformer.pkl: Este arquivo contém um objeto serializado do ColumnTransformer usado no pré-processamento dos dados.
- modelo_linear.pkl: Este arquivo contém um objeto serializado do modelo de regressão linear treinado.
- modelo_precificacao.ipynb: Este é o script principal que cria o modelo de regressão linear múltipla usando o conjunto de dados teste_indicium_precificacao.csv.
- previsao_valor_json.ipynb: Este script carrega os modelos gerados (column_transformer.pkl e modelo_linear.pkl) e faz previsões de valores com base em novos dados.
- teste_indicium_precificacao.csv: O conjunto de dados fornecido, utilizado para treinar o modelo.

--- INSTRUÇÕES DE USO ---

1) Rodando o Modelo:

Abra o script modelo_precificacao.ipynb no Google Colab ou Jupyter Notebook (feito no Google Colab).
Execute todas as células para treinar o modelo.
Os arquivos column_transformer.pkl e modelo_linear.pkl serão gerados.


2) Previsões de Valores:

Abra o script previsao_valor_json.ipynb no Google Colab ou Jupyter Notebook (feito no Google Colab).
Certifique-se de ter os arquivos column_transformer.pkl e modelo_linear.pkl na mesma pasta.
Execute as células para carregar os modelos e fazer previsões com novos dados.


--- REQUISITOS DE AMBIENTE ---
Python 3.x
Bibliotecas necessárias estão listadas no início de cada script (modelo_precificacao.ipynb e previsao_valor_json.ipynb).
Certifique-se de ter todas as dependências instaladas antes de executar os scripts.

Observação: Certifique-se de que os arquivos gerados (column_transformer.pkl e modelo_linear.pkl) estão na mesma pasta dos scripts que os utilizam.
